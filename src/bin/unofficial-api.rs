use std::env;

use actix_web::{web, App, HttpResponse, HttpServer, Result};
use chrono::prelude::*;
use unofficial_api::{Canceled, Classes, Moved, Supplementary};

fn get_jst_yyyymm() -> String {
    let dt = FixedOffset::east(9 * 3600);
    let jst_now = Utc::now().with_timezone(&dt);
    jst_now.format("%Y%m").to_string()
}

async fn get_classes() -> Result<HttpResponse, HttpResponse> {
    let yyyymm = get_jst_yyyymm();
    if let Ok(mut classes) = Classes::scrape_into_iter_parse(&yyyymm).await {
        if classes.canceled.len() > 10 {
            classes.canceled = classes.canceled.drain(0..10).collect();
        }
        if classes.moved.len() > 10 {
            classes.moved = classes.moved.drain(0..10).collect();
        }
        if classes.supplementary.len() > 10 {
            classes.supplementary = classes.supplementary.drain(0..10).collect();
        }
        Ok(HttpResponse::Ok()
            .header("Access-Control-Allow-Origin", "*")
            .json(&classes))
    } else {
        Err(HttpResponse::InternalServerError()
            .header("Access-Control-Allow-Origin", "*")
            .body("Failed to get classes."))
    }
}

async fn get_classes_canceled() -> Result<HttpResponse, HttpResponse> {
    let yyyymm = get_jst_yyyymm();
    if let Ok(mut canceled) = Canceled::scrape_into_iter_parse(&yyyymm).await {
        if canceled.len() > 10 {
            return Ok(HttpResponse::Ok()
                .header("Access-Control-Allow-Origin", "*")
                .json(&canceled.drain(0..10).collect::<Vec<Canceled>>()));
        } else {
            return Ok(HttpResponse::Ok()
                .header("Access-Control-Allow-Origin", "*")
                .json(&canceled));
        }
    }
    Err(HttpResponse::InternalServerError()
        .header("Access-Control-Allow-Origin", "*")
        .body("Failed to get canceled."))
}

async fn get_classes_moved() -> Result<HttpResponse, HttpResponse> {
    let yyyymm = get_jst_yyyymm();
    if let Ok(mut moved) = Moved::scrape_into_iter_parse(&yyyymm).await {
        if moved.len() > 10 {
            return Ok(HttpResponse::Ok()
                .header("Access-Control-Allow-Origin", "*")
                .json(&moved.drain(0..10).collect::<Vec<Moved>>()));
        } else {
            return Ok(HttpResponse::Ok()
                .header("Access-Control-Allow-Origin", "*")
                .json(&moved));
        }
    }
    Err(HttpResponse::InternalServerError()
        .header("Access-Control-Allow-Origin", "*")
        .body("Failed to get moved."))
}

async fn get_classes_supplementary() -> Result<HttpResponse, HttpResponse> {
    let yyyymm = get_jst_yyyymm();
    if let Ok(mut supplementary) = Supplementary::scrape_into_iter_parse(&yyyymm).await {
        if supplementary.len() > 10 {
            return Ok(HttpResponse::Ok()
                .header("Access-Control-Allow-Origin", "*")
                .json(&supplementary.drain(0..10).collect::<Vec<Supplementary>>()));
        } else {
            return Ok(HttpResponse::Ok()
                .header("Access-Control-Allow-Origin", "*")
                .json(&supplementary));
        }
    }
    Err(HttpResponse::InternalServerError()
        .header("Access-Control-Allow-Origin", "*")
        .body("Failed to get supplementary."))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let port = {
        if let Ok(port) = env::var("PORT") {
            port
        } else {
            String::from("8000")
        }
    };
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(HttpResponse::Ok))
            .route("/api/classes/", web::get().to(get_classes))
            .route(
                "/api/classes/canceled/",
                web::get().to(get_classes_canceled),
            )
            .route("/api/classes/moved/", web::get().to(get_classes_moved))
            .route(
                "/api/classes/supplementary/",
                web::get().to(get_classes_supplementary),
            )
    })
    .bind(format!("0.0.0.0:{}", port))?
    .run()
    .await
}
